from django.contrib import admin
from .models import Noticia
from tinymce.widgets import TinyMCE
from django.db import models

# Register your models here.
class NoticiaAdmin(admin.ModelAdmin):
    fieldsets = [
        ("Título/Data", {'fields': ["titulo", "data_publicacao", "subtitulo"]}),
        ("Conteúdo", {"fields": ["imagem","conteudo"]})
    ]
    formfield_overrides = {
        models.TextField: {'widget': TinyMCE()},
    }

admin.site.register(Noticia, NoticiaAdmin)
