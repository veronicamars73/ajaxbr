from django.db import models
from datetime import datetime

class Noticia(models.Model):
    titulo = models.CharField(max_length=150)
    subtitulo = models.CharField(max_length=350, default="")
    imagem = models.FileField(upload_to='images/', null=True, verbose_name="Imagem", default="")
    data_publicacao = models.DateTimeField('data publicacao', default=datetime.now)
    conteudo = models.TextField()

    def __str__(self):
        return self.titulo
